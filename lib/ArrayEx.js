exports.多重笛卡尔积 = arr => arr.reduce((as, bs) => as.map(a => bs.map(b => [...a, b])).flat(), [[]])
exports.数组去重 = (迭代器, 数组) => {
    var s = new Set()
    var r = []
    for (var i = 0; i < 数组.length; i++) {
        var key = 迭代器(数组[i])
        if (!s.has(key)) {
            s.add(key)
            r.push(数组[i])
        }
    }
    return r
}
