const http = require('./HttpHelp')

module.exports = async (字符串, 数量 = 5) => {
    if (字符串.indexOf('\r') != -1 || 字符串.indexOf('\n') != -1) return []
    // console.log(字符串)
    var url = `https://www.baidu.com/sugrec?prod=pc&from=pc_web&wd=${字符串}`
    var res = await http("GET")(encodeURI(url))({})
    var r = JSON.parse(res.body)
    if (r.g == null) return []
    r = r.g.map(a => a.q).filter((a, i) => i < 数量)
    // console.log(r.toString())
    return r
}
