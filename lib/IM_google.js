const http = require('./HttpHelp')

// api使用来自 https://github.com/zyctree/vscode-google-pinyin
module.exports = async (字符串, 数量 = 5) => {
    if (字符串.indexOf('\r') != -1 || 字符串.indexOf('\n') != -1) return []
    // console.log(字符串)
    var url = `http://inputtools.google.com/request?text=${字符串}&itc=zh-t-i0-pinyin&num=${数量}&cp=0&cs=1&ie=utf-8&oe=utf-8&app=demopage`
    var res = await http("POST")(encodeURI(url))({})
    // console.log(res.body)
    var r = JSON.parse(res.body)
    return r[1][0][1]
}
