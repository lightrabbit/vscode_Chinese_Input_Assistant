exports.log = (...s) => {
    if (process.env.NODE_ENV == 'dev') {
        console.log(...s.map(a => JSON.stringify(a)))
    }
    return s[s.length - 1]
}
